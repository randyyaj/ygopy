#!/usr/local/bin/python3

"""
./ygodb.py [optional_card_id]

If an id is passed in as an argument the program will look up the card information and return a json formatted object to the screen otherwise, this program queries the yugioh wikia and creates a json file **cards.json** containing all card information that is currently available. In addition a sqlite database **ygo.db** is created with both normal table (for column lookups) 
and full text search table using FTS5 (for full text search).

You can use the **ygo.db** file for Android applications.

$ sqlite3 -column -header ygo.db

    --normal table
    sqlite> select * from cards where c_name = 'Pot of Greed';

    --full text search table
    sqlite> select * from fts5_cards where fts5_cards match 'wyrm';

    sqlite> select * from fts5_cards where fts5_cards match 'raging flame';


Only supports English language for now.
"""

import sys
import os
import time
import pprint
import urllib.request
import json
import sqlite3
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime

__author__ = "Randy Yang"
__maintainer__ = "Randy Yang"
__license__ = "MIT"
__version__ = "2018.10.24.0"
__status__ = "Production"
__website__ = "https://www.gitlab.com/randyyaj"


def insert_into_cards(id, title, revisions, cards):
    """
    @param id - The card id
    @param title - The card title
    @param revisions - The wikia formatted text for pulling out card information
    @param cards - Object Dict to populate info into
    Creates a card object using provided wikia formatted revisions
    """
    card_dict = {}
    card_set = []

    for line in revisions.split("\n"):
        if "=" in line:
            split = line.split("=")
            split[0] = split[0].replace("|", "").strip()
            split[1] = split[1].strip()
            card_dict[split[0]] = split[1]

    for line in revisions.split("\n"):
        if "Card table set/header|en" in line or "Card table set/header|na" in line:
            pass
        elif "Card table set/footer" in line:
            break
        else:
            if "Card table set" in line:
                ls = line.split("|")
                card_set.append(ls[1])

    cards[id]['card_set'] = card_set

    if 'card_type' in card_dict:
        cards[id]['type'] = card_dict['card_type']
    else:
        cards[id]['type'] = 'monster'

    if 'attribute' in card_dict:
        cards[id]['attribute'] = card_dict['attribute']

    if 'atk' in card_dict:
        cards[id]['atk'] = card_dict['atk']

    if 'def' in card_dict:
        cards[id]['def'] = card_dict['def']

    if 'level' in card_dict:
        cards[id]['level'] = card_dict['level']

    if 'type' in card_dict:
        cards[id]['species'] = []
        cards[id]['species'].append(card_dict['type'])

    if 'type2' in card_dict:
        cards[id]['species'].append(card_dict['type2'])

    if 'type3' in card_dict:
        cards[id]['species'].append(card_dict['type3'])

    if 'lore' in card_dict:
        cards[id]['lore'] = card_dict['lore']

    if 'effect_types' in card_dict:
        cards[id]['effect_types'] = card_dict['effect_types']

    if 'image' in card_dict:
        cards[id]['image'] = card_dict['image']

    # pprint.pprint(cards[id])
    return card_dict


def get_card_ids():
    """
    Fetch all card ids using wikia endpoint.
    """
    url = "http://yugioh.wikia.com/api/v1/Articles/List?category=TCG_cards&limit=99999&namespaces=0"
    data_dict = {}

    with urllib.request.urlopen(url) as response:
        data = response.read()
        json_object = json.loads(data.decode('utf-8'))
        for k, v in json_object.items():
            for item in v:
                if type(item) == dict:
                    if 'id' in item:
                        data_dict[str(item['id'])] = item

    return data_dict


def get_cards():
    """
    Creates multiple threads using ThreadPoolExecutor to execute and fetch card data
    """
    cards = get_card_ids()
    ids = [*cards.keys()]
    offset = 0
    limit = 49
    page = 49
    #executor = ThreadPoolExecutor(max_workers=10)

    print(f"Downloading {len(ids)} Items...")
    start_time = time.time()

    with ThreadPoolExecutor(max_workers=10) as executor:
        while limit < len(ids):
            if (offset == 0):
                # .encode(encoding='UTF-8',errors='strict')
                query = str.join("|", ids[offset:limit])
                executor.submit(concurrent_lookup, query, cards)
            else:
                if (limit + page > len(ids)):
                    query = str.join("|", ids[offset:limit])
                    executor.submit(concurrent_lookup, query, cards)

                    query = str.join("|", ids[(offset + page): len(ids)])
                    executor.submit(concurrent_lookup, query, cards)
                else:
                    query = str.join("|", ids[offset:limit])
                    executor.submit(concurrent_lookup, query, cards)

            offset += page
            limit = limit + page

    end_time = time.time()
    print(f"Elapsed time was {end_time - start_time} seconds")
    return cards


def concurrent_lookup(query, cards):
    """
    Concurrent Thread to insert data into sqlite database
    """
    url = f"http://yugioh.wikia.com/api.php?format=json&action=query&pageids={query}&prop=revisions&rvprop=content"

    with urllib.request.urlopen(url) as response:
        data = response.read()
        json_object = json.loads(data.decode('utf-8'))

        for key in json_object['query']['pages'].keys():
            if (key != '0' or key != 0):
                if ('title' in json_object['query']['pages'][key] and 'revisions' in json_object['query']['pages'][key]):
                    title = json_object['query']['pages'][key]['title']
                    revisions = json_object['query']['pages'][key]['revisions'][0]['*']
                    insert_into_cards(key, title, revisions, cards)


def lookup(id):
    """
    Generic lookup by id function. Looks up card and creates a dictionary object containing relevant information
    """
    url = f"http://yugioh.wikia.com/api.php?format=json&action=query&pageids={id}&prop=revisions&rvprop=content"
    card_info = {str(id): get_card_ids()[str(id)]}

    with urllib.request.urlopen(url) as response:
        data = response.read()
        json_object = json.loads(data.decode('utf-8'))

        for key in json_object['query']['pages'].keys():
            if (key != '0'):
                if ('title' in json_object['query']['pages'][key] and 'revisions' in json_object['query']['pages'][key]):
                    title = json_object['query']['pages'][key]['title']
                    revisions = json_object['query']['pages'][key]['revisions'][0]['*']
                    # print(revisions)
                    #print(create_card(key, title, revisions))
                    insert_into_cards(key, title, revisions, card_info)
                    pprint.pprint(card_info[str(id)])


def write_to_file(cards, output_file_name):
    """
    Helper function to write json to output file
    """
    with open(output_file_name, 'w') as output_file:
        json.dump(obj=[*cards.values()], fp=output_file, indent=4, sort_keys=False)


def create_database(cards):
    """
    Create a sqlite3 database to use for android apps
    """
    database_file_name='ygo.db'
    os.remove(database_file_name) if os.path.exists(database_file_name) else None
    connection = sqlite3.connect(database_file_name)
    cursor = connection.cursor()
    query = open('ygodb.sql', 'r').read()

    cursor.executescript(query)

    rows = []
    for card in [*cards.values()]:
        rows.append(
            (
                card['id'] if 'id' in card else 'null',
                card['type'].replace("'", r"''") if 'type' in card else 'null',
                card['title'].replace("'", r"''") if 'title' in card else 'null',
                card['attribute'].replace("'", r"''") if 'attribute' in card else 'null',
                str.join(',', card['species']) if 'species' in card else 'null',
                card['level'].replace("'", r"''") if 'level' in card else 'null',
                card['atk'].replace("'", r"''") if 'atk' in card else 'null',
                card['def'].replace("'", r"''") if 'def' in card else 'null',
                card['effect_types'].replace("'", r"''")if 'effect_types' in card  else 'null',
                card['lore'].replace("'", r"''") if 'lore' in card else 'null',
                card['image'].replace("'", r"''") if 'image' in card else 'null',
                card['url'].replace("'", r"''") if 'url' in card else 'null',
                str.join(',', card['card_set']) if 'card_set' in card else 'null'
            )
        )
        
    sql = "insert into cards values (?,?,?,?,?,?,?,?,?,?,?,?,?)"
    cursor.executemany(sql, rows)

    sql = "insert into fts5_cards values (?,?,?,?,?,?,?,?,?,?,?,?,?)"
    cursor.executemany(sql, rows)

    print("Created sqlite3 database")
    connection.commit()
    connection.close()

if __name__ == "__main__":
    """
    If an id is passed into this program try to look up the card information based of the id and return it to the screen otherwise
    create a cards.json file containing all card information that is currently in the yugioh wikia.
    """
    if (len(sys.argv) > 1 and sys.argv[1]):
        lookup(sys.argv[1])
    else:
        cards = get_cards()
        write_to_file(cards, 'cards.json')
        create_database(cards)
