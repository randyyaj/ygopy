drop table if exists cards;
drop table if exists card_attributes;
drop table if exists card_types;
drop table if exists card_species;

create table if not exists cards (
    c_id integer primary key,
    c_type text,
    c_name text not null,
    c_attribute text,
    c_species text,
    c_level integer,
    c_atk text,
    c_def text,
    c_effect text,
    c_lore text,
    c_image text,
    c_url text,
    c_set text not null
);

create virtual table if not exists fts5_cards using fts5(c_id,c_type,c_name,c_attribute,c_species,c_level,c_atk,c_def,c_effect,c_lore,c_image,c_url,c_set);

create table if not exists card_attributes (
    c_attribute text
);

create table if not exists card_types (
    c_type text
);

create table if not exists card_species (
    c_species text
);

insert into card_attributes values ('dark');
insert into card_attributes values ('divine');
insert into card_attributes values ('earth');
insert into card_attributes values ('fire');
insert into card_attributes values ('laugh');
insert into card_attributes values ('light');
insert into card_attributes values ('water');
insert into card_attributes values ('wind');

insert into card_types values ('monster');
insert into card_types values ('spell');
insert into card_types values ('trap');

insert into card_species values ('aqua');
insert into card_species values ('beast');
insert into card_species values ('beast-Warrior');
insert into card_species values ('cyberse');
insert into card_species values ('dinosaur');
insert into card_species values ('divine-Beast');
insert into card_species values ('dragon');
insert into card_species values ('fairy');
insert into card_species values ('fiend');
insert into card_species values ('fish');
insert into card_species values ('insect');
insert into card_species values ('machine');
insert into card_species values ('plant');
insert into card_species values ('psychic');
insert into card_species values ('pyro');
insert into card_species values ('reptile');
insert into card_species values ('rock');
insert into card_species values ('sea Serpent');
insert into card_species values ('spellcaster');
insert into card_species values ('thunder');
insert into card_species values ('warrior');
insert into card_species values ('winged Beast');
insert into card_species values ('wyrm');
insert into card_species values ('zombie');