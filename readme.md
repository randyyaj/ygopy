# YGOPY

## Yugioh card downloader using python3

### Pre-requisites
- python3
- sqlite3

### Usage

```
./ygodb.py [optional_card_id]
```

If an id is passed in as an argument the program will look up the card information and return a json formatted object to the screen otherwise, this program queries the yugioh wikia and creates a json file **cards.json** containing all card information that is currently available. In addition a sqlite database **ygo.db** is created with both normal table (for column lookups) 
and full text search table using FTS5 (for full text search).

You can use the **ygo.db** file for Android applications.

```
$ sqlite3 -column -header ygo.db

--normal table
sqlite> select * from cards where c_name = 'Pot of Greed';

--full text search table
sqlite> select * from fts5_cards where fts5_cards match 'wyrm';

sqlite> select * from fts5_cards where fts5_cards match 'raging flame';
```

Only supports English language for now.